//
//  ViewController.m
//  Hobnob Ios
//
//  Created by apple on 07/06/20.
//  Copyright © 2020 AUM Telecom. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
//
@property (nonnull, strong) NSMutableArray *mArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)action:(id)sender {
    self.view.backgroundColor = [UIColor yellowColor];
}

@end
