//
//  SceneDelegate.h
//  Hobnob Ios
//
//  Created by apple on 07/06/20.
//  Copyright © 2020 AUM Telecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

